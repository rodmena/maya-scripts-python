


## by: Farsheed Ashouri  - rodmena<at>me<dot>com
'''

    //  ) )                                                            
 __//__  ___      __      ___     / __      ___      ___      ___   /  
  //   //   ) ) //  ) ) ((   ) ) //   ) ) //___) ) //___) ) //   ) /   
 //   //   / / //        \ \    //   / / //       //       //   / /    
//   ((___( ( //      //   ) ) //   / / ((____   ((____   ((___/ /

'''


import maya.OpenMaya as om
import maya.OpenMayaUI as mui
from pymel.all import *

class smartVis(object):
    '''Hide unvisible ones'''
    def __init__(self):
        '''constructor'''
        self.objects = self.selvis()
   
    def selvis(self):
        cursel = ls(sl=1)
        activeView = mui.M3dView.active3dView()
        om.MGlobal.selectFromScreen(0,0,activeView.portWidth(),activeView.portHeight(),om.MGlobal.kReplaceList)
        arg = 'createRenderLayer -name "SmartVis" -number 1 `ls -selection`;'
        mel.eval(arg)

if __name__ == '__main__':
    smart = smartVis()
    #smart.do()
    
    #smart.undo()