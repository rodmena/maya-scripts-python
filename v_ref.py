
_version = '0.1.2'
## by: Farsheed Ashouri  - rodmena<at>me<dot>com
'''

    //  ) )                                                            
 __//__  ___      __      ___     / __      ___      ___      ___   /  
  //   //   ) ) //  ) ) ((   ) ) //   ) ) //___) ) //___) ) //   ) /   
 //   //   / / //        \ \    //   / / //       //       //   / /    
//   ((___( ( //      //   ) ) //   / / ((____   ((____   ((___/ /

'''

import os
from pymel.all import *


def zeroTrans(obj):
    '''zero transformations'''
    obj.setRotation([0.0, 0.0, 0.0])
    obj.setTranslation([0.0, 0.0, 0.0])
    obj.setScale([1.0, 1.0, 1.0])


def freezeTrans(obj):
    '''undo freez transformations'''
    makeIdentity(obj, apply=True, t=1)
    

def cleanTrans(obj):
    '''undo freez transformations'''
    name = obj.name()
    translate = obj.getTranslation()
    if translate[0]==0 and translate[1]==0 and translate[2]==0:
        #~ print 'IT IS FREEEEEZED!'
        valueBefore  = getAttr(name + ".center");
        setAttr('%s.tx' % name, -valueBefore[0])
        setAttr('%s.ty' % name, -valueBefore[1])
        setAttr('%s.tz' % name, -valueBefore[2])
        freezeTrans(obj)
        #obj.setTranslation([0.0, 0.0, 0.0])
        setAttr('%s.tx' % name, valueBefore[0])
        setAttr('%s.ty' % name, valueBefore[1])
        setAttr('%s.tz' % name, valueBefore[2])


def anylize(objects):
    '''Anylize given objects and return a data block'''
    objectsBlock = dict()
    objectsInfo = dict()
    result = list()
    
    for obj in objects:
        p = obj.root()
        #~ print p
        if not p in result:
            result.append(p)
    
    for obj in result:
        wrong = False
        shapes = listRelatives(obj, ad=True, s=1)
        for shape in shapes:
            if nodeType(shape) != 'mesh':
                #~ print nodeType(shape)
                wrong = True

        if not wrong and obj:
            rotation = obj.getRotation()
            scale = obj.getScale()
            translate = obj.getTranslation()
            data = polyEvaluate(shapes, t=1, s=1, v=1, e=1)
            all_nums = []
            for each in data:
                all_nums.append(str(data[each]))
            vtx = '_'.join(all_nums)
            objname = obj.name()
            if not vtx in objectsBlock.keys():
                objectsBlock[vtx]=list()

            objectsBlock[vtx].append(obj)
            objectsInfo[objname]=(translate, rotation, scale)

    return objectsBlock, objectsInfo

    

def exp_db_refs(db, inf):
    '''Export one object of a area set as ref'''
    amount = 0
    counter = 0
    dellist = set()
    #~ progressWindow(title='Vishka Refrencer Progress',progress=amount, status='Calculating: 0 of %s meshes' % len(inf),isInterruptable=True )

    #print len(db)
    for i in db:
        refsample = db[i][0]
        #~ print i
        #print refsample
        ## Save main rotation
        if nodeType(refsample) == 'transform':
            print refsample
            #~ print 'oh yaaa'
            zeroTrans(refsample)  # freeze it's transformations
            #export
            select(refsample, r=1)
            ws = os.path.dirname(workspace(q=1, rd=1))
            refdir = '%s/Refs' % ws
            if not os.path.isdir(refdir):
                os.mkdir(refdir)
            nrntmp = '%s/%s.mb' % (refdir, refsample.replace(':','_'))
            newref = exportSelected(nrntmp , f=1)  # force export
            
            count = 1
            for obj in db[i]:
                #print obj

                ns='%s_%s' % (obj.name(), count)
                loref = createReference(newref, namespace=ns)
                #print loref
                count+=1
                dellist.add(obj)  #delete original
                
                nrn = loref.nodes()[0]
                tr, ro, sc = inf[obj.name()]
                #~ print tr, ro, sc
                nrn.setRotation(ro)
                nrn.setTranslation(tr)
                nrn.setScale(sc)
                amount +=1
                counter = (amount*100)/len(inf)
                #~ progressWindow( edit=True, progress=amount, sstatus='Calculating: %s of %s meshes' % (counter, len(inf)) )
        else:
            print 'v_ref Error: %s is not a transform node.' % refsample

    delete(dellist)  # delete originals
    #~ progressWindow(endProgress=1)


def ui():
    if window('vref', ex=1):
        deleteUI('vref')

    window('vref', t="Vishka Studio' Duplicate Referencer - version %s" % _version ,\
        mxb=0, w=500, h=200)
    showWindow()


def do():
    #ui()
    objects = ls(sl=1)
    #~ print objects
    if objects:
        db, inf = anylize(objects)
        exp_db_refs(db, inf)
    


if __name__ == '__main__':
    do()
