


## by: Farsheed Ashouri  - rodmena<at>me<dot>com
'''

    //  ) )                                                            
 __//__  ___      __      ___     / __      ___      ___      ___   /  
  //   //   ) ) //  ) ) ((   ) ) //   ) ) //___) ) //___) ) //   ) /   
 //   //   / / //        \ \    //   / / //       //       //   / /    
//   ((___( ( //      //   ) ) //   / / ((____   ((____   ((___/ /

'''



from pymel.all import *

# sets
shaderDict = dict()
shaderList = list()
shaderColorList = list()
engine = None
shader = None
lp = None
#getting data:
def tool():
    '''Nice'''
    mel.eval('hyperShadePanelMenuCommand("hyperShadePanel1", "deleteUnusedNodes")')
    shaders = ls(type='lambert')
    counter = 0
    for shader in shaders:
        #print shader
        lp = listConnections('%s.color' % shader)

        if lp and nodeType(lp[0]) != 'ramp':
        
            value = getAttr('%s.fileTextureName' % lp[0])
            print value
            
            engine = shader.listConnections(t='shadingEngine')
            hyperShade(o=shader)
            if value not in shaderColorList:
                shaderColorList.append(value)  # add color to a temp list
                shaderDict[value] = shader
            else:
                counter+=1
                delete(shader)
                delete(engine)
                delete(lp[0])
            
            pickWalk(d='up')
            objects = ls(sl=1)
            for object in objects:
                print 'Selecting %s ...' % object
                select(object, r=1)
                shader = shaderDict[value]
                if shader != 'lambert1':
                    hyperShade (assign = shader)
    
    mel.eval('hyperShadePanelMenuCommand("hyperShadePanel1", "deleteUnusedNodes")')
    print('%s duplicated shaders deleted!' % counter)

if __name__ == '__main__':
    tool()